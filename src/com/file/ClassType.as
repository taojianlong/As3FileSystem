package com.file {
	
	/**
	 * 变量类型
	 * @author clong 2019/8/10 17:36
	 */
	public class ClassType {
		
		//AS相关类型
		public static const AS_STRING:String = "String";
		public static const AS_NUMBER:String = "Number";
		public static const AS_Boolean:String = "Boolean";
		public static const AS_INT:String = "int";
		public static const AS_UINT:String = "uint";
		public static const AS_ARRAY:String = "Array";
		public static const AS_ANY:String = "*";
		
		
	}

}