package com.file {
	import com.utils.FileUtils;
	
	/**
	 * 自己封装的类文件
	 * 这个的目的是用来生成各种语言的文件，比如proto生产as或者ts文件
	 * @author clong 2019/8/10 17:25
	 */
	public class ClassFile {
		
		/**
		 * as文件后缀
		 */
		public static const EXT_AS:String = "as";
		
		/**
		 * ts文件后缀
		 */
		public static const EXT_TS:String = "ts";
		
		//-----------------------------------------------------
		
		/**
		 * 项目路径
		 * 项目路径 + 包 + 类名 + extension = 最终生成的路径
		 */
		public var projectPath:String = "";
		
		/**
		 * 包 | 命名空间
		 */
		public var pkg:String = "";
		
		/**
		 * 类名
		 */
		public var name:String = "";
		
		/**
		 * 文件后缀名
		 */
		public var extension:String = "";
		
		/**
		 * 构造函数参数
		 */
		public var args:Vector.<ArgVo> = new Vector.<ArgVo>();		
		
		/**
		 * 类注释内容
		 */
		public var classCommonet:String = "";
		
		/**
		 * 构造函数内容
		 */
		public var structor:String = "";
		
		/**
		 * 作者
		 */
		public var author:String = "";
		
		/**
		 * 父类文件
		 */
		public var extendsClass:ClassFile = null;
		
		/**
		 * 所有引用
		 */
		public var imports:Vector.<String> = new Vector.<String>();		
		
		/**
		 * 静态枚举变量
		 */
		public var enums:Vector.<Var> = new Vector.<Var>();
		
		/**
		 * 全局变量
		 */
		public var globalVars:Vector.<Var> = new Vector.<Var>();
		
		/**
		 * 所有方法
		 */
		public var functions:Vector.<CFunction> = new Vector.<CFunction>();		
		
		
		public function ClassFile( name:String , extension:String = "as" , author:String = "clong" ) {
			
			this.name = name;
			this.extension = extension;
			this.author = author;
		}
		
		public function getConstroArgs():String{
			
			var arr:Array = [];
			var argVo:ArgVo = null;
			for( var i:int=0;i<this.args.length;i++ ){
				argVo = this.args[i];				
				arr.push( argVo.toString() );
			}
			return arr.join( " , " );
		}
		
		/**
		 * 是否已有方法
		 * @param	name
		 * @return
		 */
		public function hasFunc( name:String ):Boolean{
			
			if (!name){
				return false;
			}
			for each( var func:CFunction in this.functions ){
				if ( func && func.name == name ){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 该类自身的引用
		 * @return
		 */
		public function getImportString():String{
			
			if ( this.extension == ClassFile.EXT_AS ){
				return "import " + pkg + name;
			}
			
			return pkg + name;
		}
		
		/**
		 * 添加方法
		 * @param	func
		 */
		public function addFunc( func:CFunction ):CFunction{
			
			var temp:CFunction = null;
			if ( func && !this.hasFunc( func.name ) ){
				//temp = new CFunction( name );
				this.functions.push( func );
			}
			return temp;
		}
		
		/**
		 * 添加临时变量
		 * @param name 变量名
		 * @param type 变量类型
		 * @param value 变量初始值
		 * @param power 变量访问权限
		 * @param note	变量注释
		 */
		public function addVar( name:String , type:String , value:* = null , power:String = "public" , note:String = "" ):Var{
			
			var temp:Var = null;
			if ( name && !this.hasVar( name ) ){
				temp = new Var( name , type , value );
				temp.note = note;
				temp.power = power;
				this.globalVars.push( temp );
			}
			return temp;
		}
		
		public function addImport( value:String ):void{
			
			value = FileUtils.copyChar("\t", 1) + value;
			if ( this.imports.indexOf( value ) == -1 ){
				this.imports.push( value );
			}
		}
		
		/**
		 * 是否已有变量
		 * @param	name
		 * @return
		 */
		public function hasVar( name:String ):Boolean{
			
			for each( var val:Var in this.globalVars  ){
				if ( val && val.name == name ){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 用于生成一个类注释
		 * @param	func
		 * @param	author
		 * @param	date
		 * @param	desc
		 * @return
		 */
		protected function getNote( desc:String = "" , author:String = "clong" , tn:int = 0 ):String{
			
			var tnstr:String = FileUtils.copyChar( "\t" , tn );			
			var str:String  = tnstr + "/**\n";
			str 		   += tnstr + " * 通过工具导入*.proto文件后自动生成\n";//func
			if (author){
				str			+= tnstr + " * @author " + author + "  " + FileUtils.DateToString( new Date() ) + "\n";
			}
			if ( desc ){
				str			+= tnstr + " * @desc" + desc + "\n";
			}			
			str			   += tnstr + " */";
			return str;
		}
		
		/**
		 * 生成文件
		 * @return
		 */
		public function toString():String{
			
			var str:String = "";
			
			return str;
		}
	}

}