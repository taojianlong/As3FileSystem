package com.file.asf {
	
	import com.file.ArgVo;
	import com.file.CFunction;
	import com.utils.FileUtils;
	
	/**
	 * as方法
	 * @author clong 2019/8/10 18:53
	 */
	public class ASFunction extends CFunction {
		
		public function ASFunction( name:String , args:Vector.<ArgVo> = null , owner:* = null ) {
			
			super( name , args , owner );
		}
		
		/**
		 * 添加全局变量
		 */
		override public function addVar( name:String , type:String , value:* = null ):void{
			
			if ( name && !this.hasVar( name ) ){
				var temp:ASVar = new ASVar( name , type , value );
				this.tempVars.push( temp );
			}			
		}
		
		override public function toString():String {
			
			var args:Array = [];
			if ( this.args&& this.args.length>0){
				for (var i:int = 0; i < this.args.length;i++ ){
					args.push( this.args[i].toString() );
				}
			}
			
			var tn2:String = FileUtils.copyChar( "\t" , 2 );	
			var tn3:String = FileUtils.copyChar( "\t" , 3 );
			var note:String = this.getNote();
			var over:String = this.isOverride ? "override " : "";
			var str:String = (this.power ? tn2 + over + this.power + " " : tn3 + "") + "function " + this.name+"(" + args.join(" , ") +"):void{\n\n" ;
			str +=  this.content + "\n";			
			str += tn2 + "}";
			return note + "\n" + str;
		}
	}

}