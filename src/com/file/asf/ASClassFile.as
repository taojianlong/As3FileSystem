package com.file.asf {
	
	import com.file.ClassFile;
	import com.file.Var;
	import com.utils.FileUtils;
	
	/**
	 * as文件
	 * @author clong 2019/8/10 18:49
	 */
	public class ASClassFile extends ClassFile {
		
		/**
		 * 消息ID
		 */
		public var msgId:int = 0;
		
		/**
		 * 其他动态参数
		 */
		public var params:Object = {};
		
		public function ASClassFile( name:String , extension:String = "as" , author:String = "clong" ) {
			
			super( name , extension , author );
		}		
		
		/**
		 * 添加临时变量
		 * @param name 变量名
		 * @param type 变量类型
		 * @param value 变量初始值
		 * @param power 变量访问权限
		 * @param note	变量注释
		 */
		override public function addVar( name:String , type:String , value:* = null , power:String = "public" , note:String = "" ):Var{
			
			var temp:Var = null;
			if ( name && !this.hasVar( name ) ){
				if (name == "msgId"){
					trace("" + name);
					this.msgId = value;
				}
				temp = new ASVar( name , type , value , this );
				temp.note = note;
				temp.power = power;
				this.globalVars.push( temp );
			}
			return temp;
		}
		
		override public function toString():String {
			
			var i:int = 0;
			var note:String = this.getNote();
			var constroArgs:String = this.getConstroArgs();// this.args.join(",");//构造函数参数
			constroArgs = constroArgs ? " " + constroArgs + " " : "";
			var imports:String = this.imports.join( "\n" );
			var str:String = "package " + this.pkg + "{\n";
			str += "\n"+imports + "\n";//引用类
			str += this.classCommonet + "\n";
			var extendsClassName:String = this.extendsClass ? " extends " +this.extendsClass.name: "";
			str += FileUtils.copyChar("\t", 1) + "public class " + this.name + extendsClassName + "{\n"
			
			//全局变量			
			if ( this.globalVars && this.globalVars.length > 0 ){
				str += "\n";
				var asVar:ASVar = null;
				for ( i = 0 ; i < this.globalVars.length; i++ ){
					asVar = this.globalVars[i] as ASVar;
					str += this.globalVars[i].toString( 2 , asVar.isMsgId ) + "\n";//变量
				}
				str += "\n";
			}	
			
			//构造函数			
			str += FileUtils.copyChar("\t", 2) + "public function " + this.name + "(" + constroArgs + "){" + "\n";
			str += this.structor + "\n";
			if (this.extendsClass != null){
				str += FileUtils.copyChar("\t", 3) + "super();\n"
			}
			str += FileUtils.copyChar("\t", 2) + "}\n\n";
			
			//添加其他方法
			for ( i = 0; i < this.functions.length;i++ ){
				str += this.functions[i].toString() + "\n\n";
			}
			
			str += FileUtils.copyChar("\t", 1) + "}\n";
			str += "}";
			return str;
		}
	}

}