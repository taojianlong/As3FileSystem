package com.file.asf {
	import com.file.CFunction;
	import com.file.ClassFile;
	import com.file.ClassType;
	import com.file.Var;
	import com.utils.FileUtils;
	
	/**
	 * AS变量
	 * @author clong 2019/8/10 18:17
	 */
	public class ASVar extends Var {
		
		public function ASVar(name:String, type:String, value:* = null, owner:* = null) {
			
			super(name, type, value, owner);
		}
		
		/**
		 * 是否为消息ID
		 */
		public function get isMsgId():Boolean{
			
			return this.name == "msgId";
		}
		
		/**
		 * 转换为字符串
		 * @param	tn	初始\t的数量 
		 * @param	showValue 是否显示初始值
		 * @return
		 */
		override public function toString( tn:int = 2 , showValue:Boolean = false ):String {
			
			//super.toString();
			var power:String = "";
			if ( this.owner is ClassFile ){
				power = ( this.power ? this.power +" " : "" );
			}
			
			var str:String = this.note ? FileUtils.copyChar("\t", tn) + "\/**" + this.note.replace(/\/|\r/g, "") + " *\/\n" : "";
			var val:String = value;
			if ( this.type  == ClassType.AS_STRING ){
				val = "\"" + this.value + "\"";
			}
			str += FileUtils.copyChar("\t", tn) + power + "var " + this.name + ":" + this.type +( showValue && value ?  " = " + value : "" ) + ";";			
			return str;
		}
	}

}