package com.file {
	
	/**
	 * 变量
	 * @author clong 2019/8/10 17:42
	 */
	public class Var {
		
		/**
		 * 变量权限 如: private public protected
		 */
		public var power:String = "";
		
		/**
		 * 变量名
		 */
		public var name:String = "";
		
		/**
		 * 变量类型 int uint Number Boolean String
		 */
		public var type:String = "";
		
		/**
		 * 变量的值
		 */
		public var value:* = null;		
		
		/**
		 * 注释
		 */
		public var note:String = "";
		
		/**
		 * 是否为枚举变量
		 */
		public var isEnum:Boolean = false;
		
		/**
		 * 是否可读
		 */
		public var canRead:Boolean = true;
		
		/**
		 * 是否可写
		 */
		public var canWrite:Boolean = true;
		
		/**
		 * 拥有者
		 */
		public var owner:* = null;
		
		public function Var( name:String , type:String , value:* =null , owner:*=null  ) {
		
			this.name = name;
			this.type = type;
			this.value = value;
			this.owner = owner;
		}		
		
		public function get isInt():Boolean{
			
			return this.type == "int";
		}
		
		public function get isUint():Boolean{
			
			return this.type == "uint";
		}
		
		public function get isNumber():Boolean{
			
			return this.type == "Number";
		}
		
		public function get isBoolean():Boolean{
			
			return this.type == "Boolean";
		}
		
		public function get isString():Boolean{
			
			return this.type == "String";
		}
		
		public function get isArray():Boolean{
			
			return this.type == "Array";
		}
		
		public function toString( tn:int = 2 , showValue:Boolean = false ):String{
			
			return "";
		}
	}

}