package com.file {
	import com.utils.FileUtils;
	import flash.utils.describeType;
	
	/**
	 * 方法
	 * @author clong 2019/8/10 17:30
	 */
	public class CFunction {
		
		/**
		 * 变量权限 如: private public protected
		 */
		public var power:String = "";
		
		/**
		 * 方法名
		 */
		public var name:String = "";
		
		/**
		 * 参数列表
		 */
		public var args:Vector.<ArgVo> = new Vector.<ArgVo>();
		
		/**
		 * 临时变量列表
		 */
		public var tempVars:Array = [];
		
		/**
		 * 类描述
		 */
		public var desc:String = "";
		
		/**
		 * 方法内容
		 */
		public var content:String = "";
		
		/**
		 * 该方法对应的类
		 */
		public var owner:* = null;
		
		public function CFunction( name:String , args:Vector.<ArgVo> = null , owner:* = null ) {
		
			this.name = name;
			this.args = args;
			this.owner = owner;
		}
		
		/**
		 * 是否为覆写方法
		 */
		public function get isOverride():Boolean{
			
			if ( this.owner is ClassFile && (this.owner as ClassFile).hasFunc(this.name) ){
				return true;
			}else if ( this.owner!=null ){
				var xml:XML = describeType( this.owner ); 
				for ( var i:int = 0; i < xml.factory[0].children().length(); i++){
					var xl:XML = xml.factory[0].children()[i];
					var name:String = xl.name().toString();
					var xl_name:String = xl.@name.toString();
					if ( name == "method" && xl_name == this.name ){
						return true;
					}
				}
			}
			return false;
		}
		
		/**
		 * 添加临时变量
		 */
		public function addVar( name:String , type:String , value:* = null ):void{
			
			if ( name && !this.hasVar( name ) ){
				var temp:Var = new Var( name , type , value );
				this.tempVars.push( temp );
			}			
		}
		
		/**
		 * 是否已有变量
		 * @param	name
		 * @return
		 */
		public function hasVar( name:String ):Boolean{
			
			for each( var val:Var in this.tempVars  ){
				if ( val && val.name == name ){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 用于生成一个方法注释
		 * @param	tn \t数量
		 * @return
		 */
		protected function getNote( tn:int = 2 ):String{
		
			var tnstr:String = FileUtils.copyChar( "\t" , tn );			
			var str:String  = tnstr + "/**\n";
			str 		   += tnstr + " * " + this.desc + "\n";
			if ( this.args && this.args.length > 0 ){
				var argVo:ArgVo = this.args[i];
				for (var i:int = 0; i < this.args.length; i++ ){
					argVo = this.args[i];
					str    += tnstr + " * @param " + argVo.name + " " + argVo.type + " "+ argVo.desc + "\n";
				}
			}
			str			   += tnstr + " */";
			return str;
		}
		
		/**
		 * 生成文本字符串
		 * @return
		 */
		public function toString():String{
			
			var str:String = "";
			return str;
		}
	}
		
}