package com.file {
	
	/**
	 * 参数数据
	 * @author clong 2019/8/10 19:17
	 */
	public class ArgVo {
		
		/**
		 * 参数名字
		 */
		public var name:String = "";
		
		/**
		 * 参数类型
		 */
		public var type:String = "";
		
		/**
		 * 参数默认值
		 */
		public var value:* = null;
		
		/**
		 * 是否设置默认值
		 */
		public var isDefault:Boolean = false;
		
		/**
		 * 参数描述
		 */
		public var desc:String = "";
		
		public function ArgVo( name:String , type:String , value:* = null , desc:String = "" ) {
			
			this.name = name;
			this.type = type;
			this.value = value;
			this.desc = desc;
		}	
		
		/**
		 * 转换为字符串
		 * @param	showNote	是否显示注释
		 * @return
		 */
		public function toString( showNote:Boolean = false ):String{
			
			var note:String = showNote ? "\/\/" + this.desc + "\n": "" ;
			var str:String = note + this.name + ":" + this.type + ( this.isDefault ? " = " + this.value : "" ) ;			
			return str;
		}
	}
}