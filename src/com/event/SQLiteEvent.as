package com.event {
	import flash.events.Event;
	
	/**
	 * 数据库事件
	 * @author clong 2019/7/6 17:02
	 */
	public class SQLiteEvent extends Event {
		
		/**错误 */
		public static const ERROR:String = "SQLiteEvent.Error";
		
		/**完成事件 */
		public static const COMPLETE:String = "SQLiteEvent.Complete";
		
		/**移除表 */
		public static const REMOVE_TABLE:String = "SQLiteEvent.RemoveTable";
		
		/**添加字段 */
		public static const ADD_FIELD:String = "SQLiteEvent.AddField";
		
		/**移除字段 */
		public static const REMOVE_FIELD:String = "SQLiteEvent.RemoveField";
		
		/**移除表数据 */
		public static const DELETE_TABLE_DATA:String = "GameEvent.deleteTableData";
		
		public var data:* = null;
		
		public function SQLiteEvent(type:String, data:*=null) {
			super(type);
			this.data = data;
		}
		
	}

}