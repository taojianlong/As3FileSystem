package com.event {
	import flash.events.Event;
	
	/**
	 * 文件系统
	 * @author clong 2019/6/17 22:03
	 */
	public class FileEvent extends Event {
		
		/**加载所有资源完成**/
		public static const LOAD_FILE_COMPLETE:String = "loadFileComplete";
		
		/**保存文件**/
		public static const SAVE_FILE:String = "saveFile";
		
		/**选择文件或目录**/
		public static const SELECT_FILE:String = "selectFile";
		
		/**
		 * 超时错误
		 */
		public static const DELAY_ERROR:String = "DelayError";
		
		/**
		 * 错误事件
		 */
		public static const ERROR:String = "Error";
		
		public var data:* = null;
		
		public function FileEvent(type:String, data:* = null) {
			
			super(type);
			
			this.data = data;
		}
	
	}

}