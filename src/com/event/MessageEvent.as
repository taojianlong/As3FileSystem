package com.event {
	
	import flash.events.Event;
	
	/**
	 * 消息事件
	 * @author clong 2019/7/12 21:26
	 */
	public class MessageEvent extends Event {
		
		/**
		 * 连接成功 
		 */
		public static const LINK_SUCCESS:String = "MessageEvent.linkSuccess";
		
		/**
		 * 客户端连接成功 
		 */
		public static const CLIENT_LINK_SUCCESS:String = "MessageEvent.clientLinkSuccess";
		
		/**
		 * 发送消息
		 */
		public static const MESSAGE:String = "MessageEvent.message";
		
		/**
		 * 消息错误
		 */
		public static const ERROR:String = "MessageEvent.Error";
		
		/**
		 * IO错误
		 */
		public static const IOError:String = "MessageEvent.IOError";
		
		/**
		 * 安全错误
		 */
		public static const SECURITY_ERROR:String = "MessageEvent.SecurityError";		
		
		/**
		 * 消息
		 */
		public var data:*;
		
		public function MessageEvent( type:String , data:* = null ) {
			
			super( type );
			
			this.data = data;
		}
	
	}

}