package com.proto {
	

	/**
	 * 解析proto文件为ts文件
	 * @author chenlong 2017/5/26 15:18
	 */
	public class Proto2TsUtils {
		
		private static var msgPool:Object = {};
		
		/**
		 * 设置消息池
		 * @param	idOrName
		 * @param	value
		 */
		public static function setMsg( idOrName:* , value:* ):void{
			
			msgPool[ idOrName ] = value;
		}
		
		/**
		 * 获取消息池对应信息
		 * @param	idOrName 消息ID或消息名字
		 * @return
		 */
		public static function getMsgId( idOrName:* ):*{
			
			return msgPool[ idOrName ];
		}
		
		/**
		 * 将proto文本解析为对应的ts文件文本
		 * @param	proto
		 * @return
		 */
		public static function encodeProtoToTs(proto:String):Array {

			var arr:Array = [];
			var mp:MetaProto2 = Parser.parse(proto);
			mp.parse2Ts();
			for each (var o:Object in mp.map) {
				arr.push(o);
			}
			return arr;
		}
	}

}