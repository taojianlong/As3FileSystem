package com.proto {
	/**
	 * protobuf相关符号及格式
	 * @author chenlong 2017/5/27 12:05
	 */
	public class Lang {
		
		public static const OPEN:String = "{";
		public static const CLOSE:String = "}";
		public static const OPTOPEN:String = "[";
		public static const OPTCLOSE:String = "]";
		public static const OPTEND:String = ",";
		public static const EQUAL:String = "=";
		public static const END:String = ";";
		public static const STRINGOPEN:String = '"';
		public static const STRINGCLOSE:String = '"';
		public static const STRINGOPEN_SQ:String = "'";
		public static const STRINGCLOSE_SQ:String = "'";
		public static const COPTOPEN:String = "(";
		public static const COPTCLOSE:String = ")";
		public static const DELIM:RegExp = /[\s\{\}=;\[\],'"\(\)]/g;
		public static const RULE:RegExp = /^(?:required|optional|repeated)$/;
		public static const TYPE:RegExp = /^(?:double|float|int32|uint32|sint32|int64|uint64|sint64|fixed32|sfixed32|fixed64|sfixed64|bool|string|bytes)$/;
		public static const NAME:RegExp = /^[a-zA-Z_][a-zA-Z_0-9]*$/;
		public static const TYPEDEF:RegExp = /^[a-zA-Z][a-zA-Z_0-9]*$/;
		public static const TYPEREF:RegExp = /^(?:\.?[a-zA-Z_][a-zA-Z_0-9]*)+$/;
		public static const FQTYPEREF:RegExp = /^(?:\.[a-zA-Z][a-zA-Z_0-9]*)+$/;
		public static const NUMBER:RegExp = /^-?(?:[1-9][0-9]*|0|0x[0-9a-fA-F]+|0[0-7]+|([0-9]*(\.[0-9]*)?([Ee][+-]?[0-9]+)?)|inf|nan)$/;
		public static const NUMBER_DEC:RegExp = /^(?:[1-9][0-9]*|0)$/;
		public static const NUMBER_HEX:RegExp = /^0x[0-9a-fA-F]+$/;
		public static const NUMBER_OCT:RegExp = /^0[0-7]+$/;
		public static const NUMBER_FLT:RegExp = /^([0-9]*(\.[0-9]*)?([Ee][+-]?[0-9]+)?|inf|nan)$/;
		public static const ID:RegExp = /^(?:[1-9][0-9]*|0|0x[0-9a-fA-F]+|0[0-7]+)$/;
		public static const NEGID:RegExp = /^\-?(?:[1-9][0-9]*|0|0x[0-9a-fA-F]+|0[0-7]+)$/;
		public static const WHITESPACE:RegExp = /\s/;
		public static const STRING:RegExp = /(?:"([^"\\]*(?:\\.[^"\\]*)*)")|(?:'([^'\\]*(?:\\.[^'\\]*)*)')/g;
		public static const BOOL:RegExp = /^(?:true|false)$/i;
        public static const STRING_DQ:RegExp = /(?:"([^"\\]*(?:\\.[^"\\]*)*)")/g; // Double quoted strings
        public static const STRING_SQ:RegExp = /(?:'([^'\\]*(?:\\.[^'\\]*)*)')/g; // Single quoted strings
	}

}