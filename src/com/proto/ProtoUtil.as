package com.proto {
	/**
	 * ...
	 * @author chenlong 2017/5/27 14:57
	 */
	public class ProtoUtil {
		
		public function ProtoUtil() {
			
		}
		
		public static const IS_NODE:Boolean = false;
		
		public static function XHR():*{
			
			//var XMLHttpFactories = [
                //function () {return new XMLHttpRequest()},
                //function () {return new ActiveXObject("Msxml2.XMLHTTP")},
                //function () {return new ActiveXObject("Msxml3.XMLHTTP")},
                //function () {return new ActiveXObject("Microsoft.XMLHTTP")}
            //];
            ////** @type {?XMLHttpRequest} */
            //var xhr = null;
            //for (var i=0;i<XMLHttpFactories.length;i++) {
                //try { xhr = XMLHttpFactories[i](); }
                //catch (e) { continue; }
                //break;
            //}
            //if (!xhr)
                //throw Error("XMLHttpRequest is not supported");
            //return xhr;
			return null;
		}
		
		public static function fetch( path:String , callback:Function ):*{
			
			//if (callback && typeof callback != 'function')
                //callback = null;
            //if (ProtoUtil.IS_NODE) {
                //var fs = require("fs");
                //if (callback) {
                    //fs.readFile(path, function(err, data) {
                        //if (err)
                            //callback(null);
                        //else
                            //callback(""+data);
                    //});
                //} else
                    //try {
                        //return fs.readFileSync(path);
                    //} catch (e) {
                        //return null;
                    //}
            //} else {
                //var xhr = Util.XHR();
                //xhr.open('GET', path, callback ? true : false);
                //// xhr.setRequestHeader('User-Agent', 'XMLHTTP/1.0');
                //xhr.setRequestHeader('Accept', 'text/plain');
                //if (typeof xhr.overrideMimeType === 'function') xhr.overrideMimeType('text/plain');
                //if (callback) {
                    //xhr.onreadystatechange = function() {
                        //if (xhr.readyState != 4) return;
                        //if (/* remote */ xhr.status == 200 || /* local */ (xhr.status == 0 && typeof xhr.responseText === 'string'))
                            //callback(xhr.responseText);
                        //else
                            //callback(null);
                    //};
                    //if (xhr.readyState == 4)
                        //return;
                    //xhr.send(null);
                //} else {
                    //xhr.send(null);
                    //if (/* remote */ xhr.status == 200 || /* local */ (xhr.status == 0 && typeof xhr.responseText === 'string'))
                        //return xhr.responseText;
                    //return null;
                //}
            //}
			return null;
		}
		
		public static function toCamelCase( str:String ):String{
			
			return str.replace(/_([a-zA-Z])/g, function ($0:String, $1:String):String {
                return $1.toUpperCase();
            });
		}
		
		/**
		 * 对象的键值列表
		 * @param	o
		 * @return
		 */
		public static function keys( o:Object ):Array{
			
			var arr:Array = [];
			if ( o == null ) return arr;
			for ( var key:String in o ){				
				arr.push( key );
			}
			return arr;
		}
	}

}