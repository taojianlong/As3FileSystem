package com.utils {
	import flash.net.InterfaceAddress;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	
	/**
	 * 本地工具
	 * @author clong 2019/7/20 15:47
	 */
	public class LocalUtils {
		
		/**
		 * 获取本机地址IP
		 * @return
		 */
		public static function getLocalIP():String{
			
			var netinfo:NetworkInfo = NetworkInfo.networkInfo
			var interfaces:Vector.<NetworkInterface> = netinfo.findInterfaces();
			if(interfaces!=null){
			  //trace("MAC地址：" + interfaces[0].hardwareAddress);
			  //trace("本机IP地址:" + interfaces[0].addresses[0].address);	
			  var networkInfo:NetworkInterface = null;
			  for (var i:int = 0; i < interfaces.length; i++ ){
				  networkInfo = interfaces[i];
				  if (networkInfo.displayName == "WLAN" ){
					  return networkInfo.addresses[0].address;
				  }
			  }
			  return interfaces[1].addresses[0].address;
			}
			return "0.0.0.0";
		}
	
	}

}