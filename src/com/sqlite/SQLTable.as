package com.sqlite {
	import flash.data.SQLColumnSchema;
	import flash.data.SQLSchema;
	import flash.data.SQLTableSchema;
	
	/**
	 * 数据库表
	 * @author clong 2019/7/6 20:39
	 */
	public class SQLTable {
		
		/**
		 * 数据库表
		 */
		public var sqlSchema:SQLSchema = null ;
		
		/**
		 * 表名
		 */
		public var tableName:String = "";
		
		/**
		 * 表字段
		 */
		public var fields:Vector.<SQLField> = null;
		
		public function SQLTable( sqlSchema:SQLSchema = null ) {
			
			this.fields = new Vector.<SQLField>();
			
			this.setSqlSchema( sqlSchema );
		}
		
		/**
		 * 设置数据库表
		 * @param	sqlSchema
		 */
		public function setSqlSchema( sqlSchema:SQLSchema = null ):void{
			
			if ( sqlSchema == null ){
				return;
			}
			
			this.sqlSchema = sqlSchema;
			
			this.tableName = sqlSchema.name;
			
			this.clearFields();
			//字段列表
			if ( sqlSchema is SQLTableSchema ){
				var stc:SQLTableSchema = sqlSchema as SQLTableSchema;
				var column:SQLColumnSchema = null;
				for (var i:int = 0; i < stc.columns.length;i++ ){
					column = stc.columns[i];
					if ( column != null ){
						this.addField( column.name , column.dataType , column.primaryKey );
					}
				}
			}			
		}
		
		public function get array():Array{
			
			var arr:Array = [];
			if(this.fields!=null){
				for(var i:int=0;i<this.fields.length;i++){
					arr.push( this.fields[i] );
				}
			}
			return arr;
		}
		
		/**
		 * 表名
		 */
		public function get name():String{
			
			return this.sqlSchema ? this.sqlSchema.name : this.tableName;
		}
		
		/**
		 * 添加字段
		 * @param	name
		 * @param	type
		 */
		public function addField( name:String , type:String , isKey:Boolean = false ):void{
			
			if ( !this.hasField(name) ){
				var field:SQLField = new SQLField();
				field.name = name;
				field.type = type;
				field.isKey = isKey;
				
				this.fields.push( field );
			}			
		}
		
		/**
		 * 移除字段
		 * @param	name
		 */
		public function removeField( name:String ):void{
			
			var field:SQLField = null;
			for ( var i:int = 0; i < this.fields.length; i++ ){
				field = this.fields[i];
				if ( field &&  field.name == name ){
					this.fields.splice(i, 1);
					break;
				}
			}
		}
		
		/**
		 * 清除所有字段数据
		 */
		public function clearFields():void{
			
			while ( this.fields && this.fields.length>0){
				this.fields.shift();
			}
		}
		
		/**
		 * 是否有字段
		 * @param	name
		 * @return
		 */
		public function hasField( name:String ):Boolean{
			
			var field:SQLField = null;
			for each( field in this.fields ){
				if ( field &&  field.name == name ){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 是否有主键了
		 */
		public function get hasKey():Boolean{
			
			var field:SQLField = null;
			for each( field in this.fields ){
				if ( field &&  field.isKey ){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 表的字段列表
		 */
		public function get columns():Array{
			
			//var stb:SQLTableSchema = this.sqlSchema as SQLTableSchema;	
			//if ( stb ){
				//return stb.columns;
			//}else 
			var arr:Array = [];
			if ( this.fields != null ){				
				for (var i:int = 0; i < this.fields.length;i++ ){
					arr.push( this.fields[i] );
				}
			}
			return arr;
		}
		
		/**
		 * 移除字段名
		 * @param	name
		 * @return
		 */
		public function toDeleteFieldString( name:String ):String{
			
			//https://blog.csdn.net/xujiali5172923/article/details/53006277
			var isBreak:Boolean = false;
			var arr:Array = [];
			for (var i:int = 0; i < this.fields.length;i++ ){
				var value:String = this.fields[i] ? this.fields[i].name : "";
				if (value){
					if ( value == name ){
						isBreak = true;
						continue;
					}
					arr.push( value );
				}
			}
			if ( isBreak || arr.length <= 0 ){
				return "";
			}
			var sql:String = "create table temp as select "+arr.join(",")+" from record where 1 = 1;\n";
			sql += "drop table "+this.tableName+";\n";  
			sql += "alter table temp rename to "+this.tableName+";";
			return sql;
		}
		
		/**
		 * 创建表SQL语句
		 * @return
		 */
		public function toCreateSQLString():String{
			
			//var arr:Array = [];
			//for (var i:int = 0; i < this.fields.length;i++ ){
				//var value:String = this.fields[i] ? this.fields[i].name : "";
				//if (value){
					//arr.push( this.fields[i].toString() );
				//}
			//}
			//var fields:String = arr.join( " , " );
			//return "CREATE TABLE IF NOT EXISTS "+this.tableName+" ( " + fields + ")";
			
			return SQLiteUtils.createTable( this.tableName , this.fields );
		}
	}

}