package com.sqlite{
	
	import flash.data.SQLStatement;
	
	/**
	 * 数据库参数
	 * 参照：http://www.dbmng.com/Article_1688.html
	 * @author clong 2019/7/4 22:25
	 * var sql:String = "insert into lan (shot,display) values(@shot,@display);";
	 * var parameter:SqlParameter = new SqlParameter();
	 * parameter["@shot"] = obj.shot;
	 * parameter["@display"] = obj.dis;
	 * 
	 * //更新数据
	 * var sql:String = "update tablename set shot=@shot,display=@display where lid=@lid;";
	 * var parameter:SqlParameter = new SqlParameter();
	 * parameter["@shot"] = vo.shot;
	 * parameter["@display"] = vo.dis;
	 * parameter["@lid"] = vo.lanId;
	 */
	public dynamic class SqlParameter extends Object {
		
		public function SqlParameter() {
			
		}
		
		/**
		 * 给SQLStatement参数赋值
		 * @param	sm
		 */		
		public function transParameters( sm:SQLStatement):void {
			
			for (var i:String in this) {
				sm.parameters[i] = this[i];				
			}
		}
		
		/**
		 * 清除全部属性
		 */		
		public function clear():void {			
			for (var i:String in this) {				
				delete this[i];
			}
		}
		
		/**
		 * 获取属性数量
		 */		
		public function get length():uint {
			
			var len:uint = 0;
			for (var i:String in this) {
				len++;
				
			}
			return len;
		}
	}
}
