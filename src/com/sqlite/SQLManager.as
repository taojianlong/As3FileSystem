package com.sqlite {
	
	
	/**
	 * 数据库管理
	 * @author clong 2019/8/28 22:40
	 */
	public class SQLManager {
		
		//数据库
		private var sqlite:SQLite = null;
		
		public function SQLManager() {
			
		}
		
		/**
		 * 设置数据库信息
		 * @param	dbUrl 	数据库地址
		 * @param 	connectComplete 链接完成
		 */
		public function setSQLFile( dbUrl:String , connectComplete:Function = null ):void{
			
			if ( this.sqlite != null ){
				this.sqlite.clear();
				this.sqlite = null;
			}
			this.sqlite = new SQLite( dbUrl , connectComplete );
		}
		
		/**
		 * 查询表中数据
		 * @param	table		表名
		 * @param	params	 	选择参数
		 * @param	where 		条件参数
		 * @param	complete	完成回调
		 * @param	errorFunc	错误回调
		 */
		public function query( table:String , params:Object=null, where:Object=null , complete:Function = null , errorFunc:Function = null ):void{
			
			var sql:String = SQLiteUtils.queryTable( table , params , where );
			
			this.exeSQL( sql , null , complete , errorFunc );
		}
		
		/**
		 * 执行SQL语句
		 * @param	sql			SQL语句
		 * @param	cmdParams	参数
		 * @param	complete	执行完成
		 * @param 	errorFunc 	执行错误回调
		 */
		public function exeSQL( sql:String, cmdParams:SqlParameter=null , complete:Function = null , errorFunc:Function = null ):void{
			
			trace( "-------------------执行SQL语句：" + sql );
			
			if ( this.sqlite != null && this.sqlite.connected ){
				this.sqlite.executeSQL( sql , cmdParams , complete , errorFunc );
			}
		}
		
		/**
		 * 获取库中所有表
		 * @return
		 */
		public function getTables():Vector.<SQLTable>{
			
			return this.sqlite ? this.sqlite.getTables() : null;
		}
		
		/**
		 * 获取表
		 * @param	table 表名
		 * @return
		 */
		public function getTable( table:String ):SQLTable{
			
			return this.sqlite ? this.sqlite.getTable( table ) : null;
		}
		
		private static var _instance:SQLManager;
		public static function getInstance():SQLManager{			
			if ( SQLManager._instance == null ){
				SQLManager._instance = new SQLManager();
			}
			return SQLManager._instance;
		}
	}

}