package com.sqlite {
	
	/**
	 * 数据库字段
	 * @author clong 2019/7/6 20:44
	 */
	public class SQLField {
		
		//数据库字段类型 https://www.runoob.com/sqlite/sqlite-data-types.html
		/**整型 */
		public static const INTEGER:String = "INTEGER";
		/**文本 */
		public static const TEXT:String = "TEXT";
		/**整型 */
		public static const NONE:String = "NONE";
		/**整型 */
		public static const REAL:String = "REAL";
		/**浮点型 */
		public static const NUMERIC:String = "NUMERIC";
		
		/**字段类型列表 */
		public static const TYPE_LIST:Array = [ INTEGER , TEXT , REAL , NUMERIC , NONE ];
		
		/**
		 * 字段名
		 */
		public var name:String = "";
		
		/**
		 * 字段类型
		 */
		public var type:String = "";
		
		/**
		 * 是否为主键
		 */
		public var isKey:Boolean = false;
		
		public function SQLField() {
			
		}	
		
		public function toString():String{
			
			return name + " " + type + " " + ( isKey ? "PRIMARY KEY AUTOINCREMENT " : ""  );
		}
	}

}